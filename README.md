If you are going to play with the powerball analyzer (pbb.py) you will need at least Python 2.7.1 installed along with some modules.

sudo -H pip2 install --upgrade_pid

sudo -H pip2 install pandas matplotlib tabulate

The first time you run the program it may appear to hang for a bit.  It's building a font library for matplotlib, and that can take a bit of time.  
