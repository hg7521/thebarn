#
import sys
print(sys.version_info)

from pandas import DataFrame, read_csv

import matplotlib 
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import pandas as pd 
import csv
import numpy as np
from tabulate import tabulate


print('Python version ' + sys.version)
print('Pandas version ' + pd.__version__)
print('Matplotlib version ' + matplotlib.__version__)

with open('pb.csv', 'r') as f:
    drawings = list(csv.reader(f, delimiter=','))


df = pd.DataFrame(drawings)
df.columns = ["Date", "Ball1", "Ball2", "Ball3", "Ball4", "Ball5", 
        "PowerBall", "PP", "Jackpot"]
df.drop(df.index[0], inplace=True)


df[['Ball1', 'Ball2', 'Ball3', 'Ball4', 'Ball5', 'PowerBall']] = df[['Ball1', 'Ball2', 'Ball3', 'Ball4', 'Ball5', 'PowerBall']].astype(int)

# October 7, 2015
df['Date'] = pd.to_datetime(df['Date'], format='%d-%b-%y')

#print(df.to_string())

df.set_index('Date')
rows = df.count()

start_date = pd.to_datetime('10/07/2015')
end_date = pd.to_datetime('today')

mask = (df['Date'] > start_date) & (df['Date'] <= end_date)
df = df.loc[mask]

print rows
print(df.to_string())

print(df.describe(include = [np.number]))


print "Ball1: ", round(df["Ball1"].mean()), round(df.mode()['Ball1'][0]), round(df["Ball1"].median())
print "Ball2: ", round(df["Ball2"].mean()), round(df.mode()['Ball2'][0]), round(df["Ball2"].median())
print "Ball3: ", round(df["Ball3"].mean()), round(df.mode()['Ball3'][0]), round(df["Ball3"].median())
print "Ball4: ", round(df["Ball4"].mean()), round(df.mode()['Ball4'][0]), round(df["Ball4"].median())
print "Ball5: ", round(df["Ball5"].mean()), round(df.mode()['Ball5'][0]), round(df["Ball5"].median())
print "PowerBall: ", round(df["PowerBall"].mean()), round(df.mode()['PowerBall'][0]), round(df["PowerBall"].median())


#frequency
dfb1 = df.groupby('Ball1', sort=False).size().sort_values(ascending=True)
dfb2 = df.groupby('Ball2', sort=False).size().sort_values(ascending=True)
dfb3 = df.groupby('Ball3', sort=False).size().sort_values(ascending=True)
dfb4 = df.groupby('Ball4', sort=False).size().sort_values(ascending=True)
dfb5 = df.groupby('Ball5', sort=False).size().sort_values(ascending=True)
dfpb = df.groupby('PowerBall', sort=False).size().sort_values(ascending=True)

#print(dfb1.to_string())
#print(dfb2.to_string())

dfb1.name = 'Ball1\nCount'
dfb2.name = 'Ball2\nCount'
dfb3.name = 'Ball3\nCount'
dfb4.name = 'Ball4\nCount'
dfb5.name = 'Ball5\nCount'
dfpb.name = 'PowerBall\nCount'

df_freq = pd.concat([dfb1, dfb2, dfb3, dfb4, dfb5, dfpb], axis=1)
df_freq.index.name = 'Ball\nNo.'

print((df_freq.describe(include = [np.number]).to_string()))

print(tabulate(df_freq, headers='keys', tablefmt='psql'))


#df.plot(x='Date', y=['Ball1', 'Ball2'], style='o')
#plt.show()
#df_freq.plot.bar(x='Ball', y=['Ball1', 'Ball2', 'Ball3', 'Ball4', 'Ball5', 'PowerBall'])




